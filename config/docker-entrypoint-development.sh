#!/usr/bin/env bash

#exec gunicorn --bind 0.0.0.0:8000 engine.wsgi:application --reload

exec daphne -b 127.0.0.1 -p 8000 engine.asgi:application