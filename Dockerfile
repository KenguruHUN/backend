FROM alpine:latest

LABEL maintainer = "Csókás Gergő <gergo.csokas@gmail.com>"

ENV ENV_PATH /etc/engine/
ENV PATH /opt/engine/src:$PATH
ENV PYTHONPATH /usr/bin/python3
ENV PYTHONUNBUFFERED 1

VOLUME /opt/engine
VOLUME /opt/sockets
VOLUME /srv/engine

RUN apk update && apk upgrade --no-cache

RUN apk add --no-cache \
            python3 \
            postgresql-dev && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --upgrade pip setuptools && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
    rm -r /root/.cache && \
    apk add --no-cache --virtual .build-deps \
            alpine-sdk \
            python3-dev \
            zlib-dev \
            libffi-dev \
            jpeg-dev

COPY requirements /opt/engine/requirements
RUN python3 -m pip install --no-cache-dir -r /opt/engine/requirements/base.txt

RUN apk del .build-deps

COPY .env_example /etc/engine/.env

COPY config/docker-entrypoint.sh /opt/engine/
COPY tasks /opt/engine/tasks
COPY src /opt/engine/src

WORKDIR /opt/engine/src

COPY config/gunicorn /etc/gunicorn/conf

CMD sh /opt/engine/docker-entrypoint.sh

