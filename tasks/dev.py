# -*- coding: utf-8 -*-
from invoke import task

from .helpers import root_folder

root_path = root_folder()


@task
def collect_static(ctx, verbosity=0):
    """
    Collect static files.

    :param ctx: contextualized=true boilerplate of Invoke

    :param verbosity: verbosity level default 0, max level 2
    :type verbosity: integer

    :return: None
    """

    ctx.run('python3 manage.py collectstatic --noinput --verbosity {}'.format(verbosity))


@task
def migrate(ctx, make=False, rebuild=False):
    """
    Migrate models to the database

    :param ctx: contextualized=true boilerplate of Invoke

    :param make: if true run with make migrations
    :type make: boolean

    :param rebuild: if true, find and delete all migration file and reseting the database
    :type rebuild: boolean

    :return: None
    """
    if rebuild:
        ctx.run('find {} -name "000*" -delete'.format(root_path))
        ctx.run('python3 manage.py reset_db')

    if make:
        ctx.run('python3 manage.py makemigrations')

    ctx.run('python3 manage.py migrate')
