# -*- coding: utf-8 -*-
from django.conf.urls import include
from django.urls import path
from rest_framework import routers

from api.v0 import views

router = routers.DefaultRouter()
router.register(r'page', views.SinglePageViewset)
router.register(r'category', views.CategoryViewSet)
router.register(r'blog', views.BlogViewSet)

urlpatterns = [
    path('v0/', include(router.urls)),
    path('auth/', include('rest_framework.urls')),
]