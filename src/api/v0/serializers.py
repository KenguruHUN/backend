# -*- coding: utf-8 -*-
from rest_framework import serializers

from core.models import SinglePage
from blog.models import Post, Category, Tag


class SinglePageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SinglePage
        fields = ('url', 'title', 'content')
        lookup_field = 'filter_url'
        extra_kwargs = {
            'url': {'lookup_field': 'filter_url'}
        }


class CategorySerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Category
        fields = ('name', 'color', 'slug')


class TagSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Tag
        fields = ('name',)


class PostListSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Post
        fields = ('url', 'is_public', 'title')
        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }


class PostDetailSerializer(serializers.HyperlinkedModelSerializer):

    category = CategorySerializer(many=False)
    tags = TagSerializer(many=True)

    class Meta:
        model = Post
        fields = ('title', 'category', 'tags', 'content', )
