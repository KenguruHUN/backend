# -*- coding: utf-8 -*-
from rest_framework import viewsets

from api.v0.serializers import SinglePageSerializer, CategorySerializer, \
    TagSerializer, PostListSerializer, PostDetailSerializer
from blog.models import Category, Tag, Post
from core.models import SinglePage


class SinglePageViewset(viewsets.ModelViewSet):
    queryset = SinglePage.objects.all()
    serializer_class = SinglePageSerializer
    lookup_field = 'filter_url'


class CategoryViewSet(viewsets.ModelViewSet):

    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class TagListViewSet(viewsets.ModelViewSet):

    queryset = Tag.objects.all()
    serializer_class = TagSerializer


class BlogViewSet(viewsets.ModelViewSet):

    queryset = Post.objects.select_related('category').select_related('author').prefetch_related('tags').distinct()
    serializer_class = PostListSerializer
    detail_serializer_class = PostDetailSerializer
    lookup_field = 'slug'

    def get_serializer_class(self):
        if self.action == 'retrieve':
            if hasattr(self, 'detail_serializer_class'):
                return self.detail_serializer_class

        return super(BlogViewSet, self).get_serializer_class()
