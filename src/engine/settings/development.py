# -*- coding: utf-8 -*-
from .base import *

logger = logging.getLogger(__name__)

env = environ.Env(
    DEBUG=(bool, True),
)

DEBUG = env('DEBUG')

TEST_RUNNER = ''

SESSION_EXPIRE_AT_BROWSER_CLOSE = ''

INTERNAL_IPS = ['*']

INSTALLED_APPS += []

MIDDLEWARE += []

STATIC_ROOT = ''  # os.path.join(BASE_DIR, 'static_root')

