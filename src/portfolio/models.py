# -*- coding: utf-8 -*-
from django.db import models
from django.utils.text import slugify
from django.contrib.postgres.fields import ArrayField
from django.utils.translation import ugettext_lazy as _

from martor.models import MartorField


class Tag(models.Model):
    """Tag model."""

    name = models.CharField(_('tag'), max_length=25, blank=True)

    class Meta:
        ordering = ['-name']

    def __str__(self):
        return self.name


class TimeStampModel(models.Model):
    """ Time stamp abstract model."""

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class ContentModel(TimeStampModel):
    """ Content abstract model."""

    description = MartorField(
        _('content')
    )

    class Meta:
        abstract = True


class Project(ContentModel):
    """ Portfolio project model."""

    def image_path(self, instance, filename):
        return f"uploads/projects/{slugify(instance.title)}/{filename}"

    title = models.CharField(_('project title'), max_length=100)
    slug = models.SlugField(_('project title slug'), max_length=105, blank=True, unique=True)
    tags = models.ManyToManyField('Tag')
    technologies = ArrayField(_('technologies in the project'), models.CharField(max_length=25))
    head_image = models.ImageField(upload_to=image_path, default='no-image')
    activities = ArrayField(models.CharField(max_length=100), default=list)
    images = ArrayField(models.ImageField(upload_to=image_path, default=None), default=list)
    link = models.URLField(_('project link'))

    class Meta:
        unique_together = ('slug', 'created')

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        return super(Project, self).save(*args, **kwargs)
