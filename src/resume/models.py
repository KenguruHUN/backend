from django.db import models
from django.contrib.postgres.fields import DateRangeField
from django.utils.translation import ugettext_lazy as _

from martor.models import MartorField


class BasicData(models.Model):
    """ General information model."""
    name = models.CharField(_('name'), max_length=120)
    birth_date = models.DateField(_('date of birth'))
    email = models.EmailField(_('e-mail address'))
    phone_number = models.CharField(_('phone number'), max_length=10)
    nationality = models.CharField(_('nationality'), max_length=50)


class TechnicalInformation(models.Model):
    """ Technical summary model."""
    category = models.CharField(_('skill category'), max_length=25)
    # type = models.


class PersonalInformation(models.Model):
    pass


class Position(models.Model):
    name = models.CharField(_('name of the position'), max_length=100)


class WorkExperience(models.Model):
    from_to = DateRangeField()
    # company_name = models.CharField(_(''), max_length=)
    company_url = models.URLField(_(''))
    position = models.ForeignKey('Position', on_delete=models.CASCADE)
    activities = MartorField(_('main activities and responsibilities'))
    technologies = models.ManyToManyField('TechnicalInformation', limit_choices_to={'type': 'skill'})
    pass


class EducationAndTraining(models.Model):
    pass
